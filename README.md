**Domain Assessment - Proposed Solution**

Mohammad Eyasin Arafat


**Pre-requisite**
User will need to install node js in the windows/mac

**Steps to run the tests UI**
*After downloading the solution folder

1. Please open the folder "Domain" in VS code / Terminal (Inside that all the folder will be available)
2. run the command - npm install cypress
3. run the command - npm install cypress-xpath

*To run the tests from UI 

1. run the command - npx cypress open

The terminal/vs code will open the UI for running cypress tests (visually)

**Running tests from terminal**
A reporting system has been inegrated so before running the tests in the terminal the follwoing command need to be run 

1. npm install cypress-multi-reporters
2. npm install mocha
3. npm install mochawesome
4. npm install mochawesome-merge
5. npm install mochawesome-report-generator

*Then to run the tests from vs code
 
1. run the command - npx cypress run --spec "cypress/integration/Domain/*.*"

This will also geberate a Html report that can be found in this ("cypress\reports\mocha") location





