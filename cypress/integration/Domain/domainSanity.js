
import homePage from '../..//pageObjects/homePage'


const url = Cypress.env('url')

beforeEach(()=>{
    cy.visit((url),{headers: {"Accept-Encoding": "gzip, deflate"}})
})

describe('Navigating to deferent menu items',() =>{

    it('Can vefiry Buy Menu',()=>{

        homePage.navigateToBuy()

        homePage.verifyBuyMenu()

    })

    it('Can vefiry Rent menu',()=>{

        homePage.navigateToRent()

        homePage.verifyRentMenu()

    })


    it('Can vefiry House & Land Menu',()=>{

        homePage.navigateToHouseAndLand()

        homePage.verifyHouseAndLandMenu()

    })

    it('Can vefiry Sold Menu',()=>{

        homePage.navigateToSold()

        homePage.verifySoldMenu()

    })

    it('Can vefiry New Homes Menu',()=>{

        homePage.navigateToNewHomes()

        homePage.verifyNewHomesMenu()

    })

    it('Can vefiry Retirement Menu',()=>{

        homePage.navigateToRetirement()

        homePage.verifyRetirementMenu()

    })

    it('Can vefiry Rural page',()=>{

        homePage.navigateToRural()

        homePage.verifyRuralMenu()

    })

describe('Can search for sydney listings',() =>{

        it('It can search for Sydney listings',()=>{

            homePage.searchForKeyword('Sydney')
    
            homePage.verifylisitings()
    
        })

    })

})