
/* Navigation Menu Items  */ 

const btnBuy = '//button[contains(text(),"Buy")]'
const btnRent = '//button[contains(text(),"Rent")]'
const btnHouseAndLand = '//button[contains(text(),"House & Land")]'
const btnNewHomes = '//button[contains(text(),"New Homes")]'
const btnSold = '//button[contains(text(),"Sold")]'
const btnRetirement = '//button[contains(text(),"Retirement")]'
const btnRural = '//button[contains(text(),"Rural")]'

/* ----------------------- */

/* Verification Elements  */ 

const verificationBuy= '//h2[contains(text(),"Buy for under")]'
const verificationRent = '//a[contains(text(),"How to secure a bargain when moving to a new renta")]'
const verificationHouseAndLand = '//h2[contains(text(),"Start your house and land adventure with these mus")]'
const verificationNewHomes = '//h2[contains(text(),"New Developments")]'
const verificatioinRetirement = '//h2[contains(text(),"Explore retirement homes & properties for sale")]'
const verificationRural = '//a[contains(text(),"Regional towns where house prices shot up 30 per")]'

/* ----------------------- */


/* element for search operation */

const searchBox = 'input[placeholder="Try a location or a school or project name "]'
const seachBtn = 'button[data-testid="search-button"]'

/* ---------------------------- */ 

/* verification search operation */

const sydneyListing = '//h1[contains(text(),"for sale in Sydney")]' 

/* ----------------------------- */


class homePage
{

/* Navigations Methods  */ 

static navigateToBuy()
{
    cy.xpath(btnBuy).click()
}
static navigateToRent()
{
    cy.xpath(btnRent).click()
}
static navigateToHouseAndLand()
{
    cy.xpath(btnHouseAndLand).click()
}
static navigateToNewHomes()
{
    cy.xpath(btnNewHomes).click()
}
static navigateToSold()
{
    cy.xpath(btnSold).click()
}
static navigateToRetirement()
{
    cy.xpath(btnRetirement).click()
}
static navigateToRural()
{
    cy.xpath(btnRural).click()
}
/* ----------------------- */

/* Verification Methods    */

static verifyBuyMenu()
{
    cy.xpath(verificationBuy).should('exist')
}

static verifyRentMenu()
{
    cy.scrollTo(0, 500)
    cy.xpath(verificationRent,{timeout:25000}).should('exist')
    cy.url().should('contain','mode=rent')
}

static verifyHouseAndLandMenu()
{
    cy.scrollTo(0, 500)
    cy.xpath(verificationHouseAndLand,{timeout:25000}).should('exist')
    cy.url().should('contain','house-and-land')
}

static verifySoldMenu()
{
    cy.url().should('contain','?mode=sold')
}

static verifyNewHomesMenu()
{
    cy.scrollTo(0, 500)
    cy.xpath(verificationNewHomes,{timeout:25000}).should('exist')
    cy.url().should('contain','new-homes')
}

static verifyRetirementMenu()
{
    cy.scrollTo(0, 500)
    cy.xpath(verificatioinRetirement,{timeout:25000}).should('exist')
    cy.url().should('contain','retirement')
}

static verifyRuralMenu()
{
    cy.scrollTo(0, 500)
    cy.xpath(verificationRural,{timeout:25000}).should('exist')
    cy.url().should('contain','rural')
}

/* ----------------------- */

/* Method for search operation */

static searchForKeyword(city)
{
    cy.get(searchBox).type(city)
    cy.get(seachBtn).click()
}

/* ----------------------- */

/* Verifiy search operation */

static verifylisitings()
{
    cy.xpath(sydneyListing,{timeout:25000}).should('exist')
}

}
export default homePage